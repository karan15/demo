const properties = [
  {
    id: 1,
    title: "Nearby Getaways",
    description: "qwerty 1",
    isNew: true,
    location: "Delhi",
  },
  {
    id: 2,
    title: "Online Experiences",
    description: "qwerty 1",
    isNew: false,
    location: "Bangalore",
  },
  {
    id: 3,
    title: "Entire Homes",
    description: "qwerty 1",
    isNew: true,
    location: "Noida",
  },
  {
    id: 4,
    title: "Nearby Hello",
    description: "qwerty 1",
    isNew: true,
    location: "Gurugram",
  },
];

export default properties;

const footerContent = {
  stayInformed: {
    title: "Stay Informed",
    forGuests: {
      title: "For Guest",
      content: [
        {
          title: "Update for travel",
          description: "What you should know",
        },
        {
          title: "Cancellation option",
          description: "Learn what's covered",
        },
        {
          title: "Help centre",
          description: "Get supported",
        },
      ],
    },
    forHost: {
      title: "For Host",
      content: [
        {
          title: "Update for travel",
          description: "what you should know",
        },
        {
          title: "Cancellation option",
          description: "Learn what's covered",
        },
        {
          title: "Help centre",
          description: "Get supported",
        },
      ],
    },
    covid: {
      title: "For Covid-19",
      content: [
        {
          title: "Update for travel",
          description: "what you should know",
        },
        {
          title: "Cancellation option",
          description: "Learn what's covered",
        },
        {
          title: "Help centre",
          description: "Get supported",
        },
      ],
    },
    others: {
      title: "Others",
      content: [
        {
          title: "Update for travel",
          description: "what you should know",
        },
        {
          title: "Cancellation option",
          description: "Learn what's covered",
        },
        {
          title: "Help centre",
          description: "Get supported",
        },
      ],
    },
  },
  links: [{ id: 1, url: "https://www.twitter.com" }],
};

export default footerContent;

import React from "react";

const Button = (props) => {
  return (
    <div>
      <div>
        <button onClick={props.onClick}>{props.label}</button>
      </div>
    </div>
  );
};

export default Button;

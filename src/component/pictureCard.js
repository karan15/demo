import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";

const useStyles = makeStyles({
  root: {
    width: "100%",
    height: "100%",
    borderRadius: 10,
    position: "relative",
    backgroundColor: "transparent",
  },
});

export default function PictureCard(props) {
  const classes = useStyles();
  let heightOfContent = props.description ? 100 : 60;
  return (
    <Card
      elevation={3}
      className={classes.root}
      onClick={() => {
        props.navigateTo(props.id);
      }}
      onMouseEnter={() => {
        console.log("mouse enter");
      }}
      onMouseLeave={() => {
        console.log("mouse leave");
      }}
    >
      <CardActionArea style={{ height: "100%" }}>
        <CardMedia
          className={classes.media}
          style={{ height: `calc(100% - ${heightOfContent}px)` }}
          image="https://picsum.photos/200/300"
          title="Contemplative Reptile"
        />
        <CardContent
          style={{ ...props.cardContentStyle, height: heightOfContent }}
        >
          <Typography gutterBottom variant="h5" component="h2">
            {props.title}
          </Typography>
          {props.description ? (
            <Typography variant="body2" color="textSecondary" component="p">
              {props.description}
            </Typography>
          ) : null}
        </CardContent>
      </CardActionArea>
      {props.isNew ? (
        <Typography
          style={{
            position: "absolute",
            top: 10,
            left: 10,
            paddingLeft: 8,
            paddingRight: 8,
            paddingTop: 3,
            paddingBottom: 3,
            backgroundColor: "#fff",
            fontWeight: 500,
            fontSize: 10,
            borderRadius: 5,
          }}
        >
          NEW
        </Typography>
      ) : null}
    </Card>
  );
}

const defaultProps = {
  title: "Edit Title",
};

const propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  isNew: PropTypes.bool.isRequired,
};

PictureCard.defaultProps = defaultProps;
PictureCard.propTypes = propTypes;

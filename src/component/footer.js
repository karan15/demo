import React from "react";
import footerContent from "../content/footer";
import { Grid, Typography } from "@material-ui/core";

const Footer = () => {
  return (
    <Grid style={{}}>
      <Grid container>
        <StayInformed stayInformedData={footerContent.stayInformed} />
      </Grid>
    </Grid>
  );
};

const StayInformed = (props) => {
  console.log(props);
  return (
    <Grid item sm={12}>
      <Typography>{props.stayInformedData.title}</Typography>
      <Grid container>
        <Grid item sm={3}>
          <RenderList forGuestsData={props.stayInformedData.forGuests} />
        </Grid>
        <Grid item sm={3}>
          <RenderList forGuestsData={props.stayInformedData.forHost} />
        </Grid>
        <Grid item sm={3}>
          <RenderList forGuestsData={props.stayInformedData.covid} />
        </Grid>
        <Grid item sm={3}>
          <RenderList forGuestsData={props.stayInformedData.others} />
        </Grid>
      </Grid>
    </Grid>
  );
};

const RenderList = (props) => {
  return (
    <Grid>
      <Grid style={{ borderBottom: "1px solid black", marginBotton: 15 }}>
        <Typography style={{ fontSize: 20 }}>
          {props.forGuestsData.title}
        </Typography>
      </Grid>
      {props.forGuestsData.content.map((item, index) => {
        return (
          <Grid key={index}>
            <Grid style={{ borderBottom: "1px solid black" }}>
              <Typography style={{ fontWeight: 600 }}>{item.title}</Typography>
              <Typography>{item.description}</Typography>
            </Grid>
          </Grid>
        );
      })}
    </Grid>
  );
};

export default Footer;

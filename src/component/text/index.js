import React, { Component } from "react";

class Text extends Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: 1,
    };
    this.handleClick1 = this.handleClick1.bind(this);
  }

  handleClick = () => {
    this.setState({ counter: this.state.counter + 1 });
  };

  handleClick1() {
    this.setState({ counter: this.state.counter + 1 });
  }

  componentDidMount() {
    console.log("component did mount");
  }

  shouldComponentUpdate() {
    if (this.state.counter === 5) {
      return false;
    }
    return true;
  }

  componentDidUpdate() {
    console.log("component did update");
  }

  componentWillUnmount() {}

  render() {
    return (
      <div>
        <p onClick={this.handleClick}>{this.state.counter}</p>
        <p>{this.props.label}</p>
      </div>
    );
  }
}

export default Text;

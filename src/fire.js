import firebase from "firebase";

var firebaseConfig = {
  apiKey: "AIzaSyDPPEQflyTBqankvsg21hxlCi5KrqFpqVQ",
  authDomain: "chatting-app-live-7087b.firebaseapp.com",
  databaseURL: "https://chatting-app-live-7087b.firebaseio.com",
  projectId: "chatting-app-live-7087b",
  storageBucket: "chatting-app-live-7087b.appspot.com",
  messagingSenderId: "429395547294",
  appId: "1:429395547294:web:6bacad1699cb6f3188978f",
  measurementId: "G-JB66NCPR8X",
};

const fire = firebase.initializeApp(firebaseConfig);

export default fire;

import React, { useState, useEffect } from "react";
import { Grid, Card, Typography, TextField, Button } from "@material-ui/core";
import fire from "../../fire";

const CustomerSupport = (props) => {
  const [message, setMessage] = useState("");
  const [messages, setMessages] = useState([]);
  const [noData, setNoData] = useState(false);

  useEffect(() => {
    console.log("component mounted");
    readDataFromFirebase();
  }, []);

  useEffect(() => {
    console.log("component update on message update");
  }, [message]);

  const handleMessage = (e) => {
    setMessage(e.target.value);
  };
  const sendMessage = () => {
    console.log(message);
    fire.database().ref("/messages").push({
      message: message,
      user: "Karan",
      timestamp: new Date(),
    });
    setMessage("");
  };

  const readDataFromFirebase = () => {
    fire
      .database()
      .ref("/messages")
      .on("value", (data) => {
        let messagesFromServer = data.val();
        if (!messagesFromServer) {
          setNoData(true);
        }
        let messagesArray = [];
        for (let key in messagesFromServer) {
          messagesArray.push(messagesFromServer[key]);
        }
        setMessages(messagesArray);
      });
  };

  return (
    <>
      <Grid container style={{ height: "100vh" }}>
        <Grid item sm={7}>
          <Card style={{ height: "100%" }}>
            <Typography variant={"h2"}>We are here to help!!</Typography>
            <Typography variant={"body1"}>
              Available between 9am to 6pm (IST). Closed on weekends and public
              holidays
            </Typography>
          </Card>
        </Grid>
        <Grid item sm={5}>
          <Grid style={{ position: "relative", height: "100%" }}>
            <Grid>
              <Typography variant={"h3"}>Chat With Us</Typography>
            </Grid>
            <Grid
              id="display"
              style={{ height: "80vh", overflow: "auto", margin: 10 }}
            >
              {messages.length > 0 ? (
                <>
                  {messages.map((msg, i) => {
                    return <Typography>{msg.message}</Typography>;
                  })}
                </>
              ) : (
                <>
                  {noData ? (
                    <Typography>No new messages</Typography>
                  ) : (
                    <Typography>Loading</Typography>
                  )}
                </>
              )}
            </Grid>
            <Grid
              id="bottomActionBar"
              style={{
                position: "absolute",
                bottom: 20,
                width: "100%",
                backgroundColor: "#f1f6d2",
                border: "1px solid black",
              }}
            >
              <Grid container spacing={1} alignItems="center">
                <Grid item sm={8}>
                  <TextField
                    type="text"
                    value={message}
                    label="Message"
                    style={{ width: "100%" }}
                    variant="outlined"
                    onChange={handleMessage}
                  />
                </Grid>
                <Grid item sm={4}>
                  <Button
                    variant="contained"
                    onClick={sendMessage}
                    style={{ width: "100%" }}
                  >
                    Send
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default CustomerSupport;

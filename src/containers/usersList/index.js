import React, { useState, useEffect } from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { getUsers } from "../../redux/action";

const UserList = (props) => {
  const store = useSelector((store) => store);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getUsers());
  }, []);
  return (
    <>
      <div>Users List</div>
      {store.users.length > 0 ? (
        <>
          {store.users.map((user, i) => {
            return <p key={i}>{user.email}</p>;
          })}
        </>
      ) : (
        <>{store.loading ? <p>Loading</p> : <p>No Data Found</p>}</>
      )}
    </>
  );
};

export default UserList;

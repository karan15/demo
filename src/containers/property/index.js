import React from "react";
import Header from "../../component/header";
import propertiesContent from "../../content/properties";
import { Grid, Typography, Card } from "@material-ui/core";

const Property = (props) => {
  console.log(props);

  const selectedProperty = propertiesContent.filter((property, index) => {
    return property.id === parseInt(props.match.params.id);
  });
  console.log(selectedProperty);
  return (
    <>
      <Header />
      <Grid style={{ padding: 20 }}>
        <h1>Property</h1>
        {selectedProperty.length > 0 ? (
          <Card style={{ padding: 20 }}>
            <Grid container spacing={3}>
              <Grid item>
                <Typography>Title: </Typography>
                <Typography>{selectedProperty[0].title}</Typography>
              </Grid>
              <Grid item>
                <Typography>Description: </Typography>
                <Typography>{selectedProperty[0].description}</Typography>
              </Grid>
              <Grid item>
                <Typography>Location: </Typography>
                <Typography>{selectedProperty[0].location}</Typography>
              </Grid>
            </Grid>
          </Card>
        ) : (
          <Grid>
            <Typography>Property not available</Typography>
          </Grid>
        )}
      </Grid>
    </>
  );
};

export default Property;

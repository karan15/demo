import React, { useState, useEffect } from "react";
import Header from "../../component/header";
import { Grid, Typography, Button } from "@material-ui/core";
import PropTypes from "prop-types";
import styles from "./styles";
import PictureCard from "../../component/pictureCard";
import propertiesContent from "../../content/properties";
import Footer from "../../component/footer";
import fire from "../../fire";
import { useSelector } from "react-redux";

const Home = (props) => {
  const [counter, setCounter] = useState(0);
  const [list, updateList] = useState([]);
  const [name, setName] = useState("");

  useEffect(() => {
    fire.auth().onAuthStateChanged(function (user) {
      if (user) {
        // User is signed in.
        var displayName = user.displayName;
        var email = user.email;
        var emailVerified = user.emailVerified;
        var photoURL = user.photoURL;
        var isAnonymous = user.isAnonymous;
        var uid = user.uid;
        var providerData = user.providerData;
        console.log(user.displayName);
        // ...
      } else {
        props.history.push("/login");
        // User is signed out.
        // ...
      }
    });
  }, []);

  const handleClick = () => {
    setCounter(counter + 1);
    console.log(counter);
  };
  console.log(props);
  const navigateTo = (propertyId) => {
    props.history.push(`/property/${propertyId}`);
  };
  console.log(fire.database());
  const storeCounter = useSelector((store) => store.counter);
  return (
    <>
      <Header history={props.history} />
      {storeCounter}
      <Grid style={{ marginLeft: 25, marginRight: 25 }}>
        <Grid container style={{ marginTop: 40 }}>
          <Grid item xs={12} sm={6} md={6}>
            <Typography style={styles.title} variant={"h3"}>
              You don't need to go far to find what matters.
            </Typography>
          </Grid>
        </Grid>
        <Grid container spacing={2} style={{ marginTop: 25, minHeight: 300 }}>
          {propertiesContent.map((val, index) => {
            return (
              <Grid key={index} item sm={6} md={3} xs={12}>
                <PictureCard
                  navigateTo={navigateTo}
                  title={val.title}
                  description={val.description}
                  isNew={val.isNew}
                  id={val.id}
                />
              </Grid>
            );
          })}
        </Grid>
      </Grid>
      <Grid style={styles.onlineExperienceContainer}>
        <Grid container style={{ marginBottom: 50 }}>
          <Grid item sm={6}>
            <Typography
              variant={"h4"}
              style={{ color: "#fff", marginBottom: 10 }}
            >
              Online Experience
            </Typography>
            <Typography
              variant={"body1"}
              style={{ color: "#fff", opacity: 0.7 }}
            >
              Meet people all over the world while trying something new. Join
              live, interactive video sessions led by one-of-a-kind hosts – all
              without leaving home.
            </Typography>
          </Grid>
        </Grid>
        <Grid container style={{ height: "80vh" }}>
          <Grid item sm={6} style={{ paddingRight: 10 }}>
            <PictureCard title={"Hello"} cardContentStyle={styles.darkCard} />
          </Grid>
          <Grid item sm={6}>
            <Grid container style={{ height: "50%", paddingBottom: 10 }}>
              <Grid item sm={6} style={{ paddingRight: 10 }}>
                <PictureCard
                  title={"Hello"}
                  cardContentStyle={styles.darkCard}
                />
              </Grid>
              <Grid item sm={6}>
                <PictureCard
                  title={"Hello"}
                  cardContentStyle={styles.darkCard}
                />
              </Grid>
            </Grid>
            <Grid container style={{ height: "50%" }}>
              <Grid item sm={12}>
                <PictureCard
                  title={"Hello"}
                  cardContentStyle={styles.darkCard}
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>

        <Footer />
      </Grid>
    </>
  );
};

export default Home;

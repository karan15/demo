const styles = {
  title: {
    color: "#3f51b5",
  },
  onlineExperienceContainer: {
    backgroundColor: "black",
    padding: 25,
    marginTop: 30,
    height: "100vh",
  },
  darkCard: {
    backgroundColor: "rgb(34, 34, 34)",
    color: "rgb(255, 255, 255)",
  },
};

export default styles;

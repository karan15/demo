import React, { useEffect, useState } from "react";
import fire from "../../fire";
import AceEditor from "react-ace";
import Text from "../../component/text";

import "ace-builds/src-noconflict/mode-java";
import "ace-builds/src-noconflict/mode-javascript";
import "ace-builds/src-noconflict/theme-github";

import { Grid, Typography, Button } from "@material-ui/core";

import Header from "../../component/header";

export const CodeCollab = (props) => {
  const [code, setCode] = useState("");
  const [currentUser, setCurrentUser] = useState("");
  const [mode, setMode] = useState("java");

  useEffect(() => {
    readDatabase();
    let timerRef = setInterval(() => {
      console.log(1000);
    }, 2000);
    console.log("created");
    return () => {
      clearInterval(timerRef);
    };
  }, []);

  useEffect(() => {
    console.log("mode changed");
  }, [mode]);

  useEffect(() => {
    console.log("code changed");
  }, [code]);

  function onChange(newValue) {
    fire.database().ref("codeCollab").set({ code: newValue, user: "karan" });
  }

  const readDatabase = () => {
    fire
      .database()
      .ref("codeCollab")
      .on("value", (data) => {
        setCode(data.val().code);
        setCurrentUser(data.val().user);
      });
  };

  const handleLanguage = (language) => {
    if (language === "java") {
      setMode("java");
      setCode("System.out.println('hello world')");
    } else {
      setMode("javascript");
      setCode("// bake your code here");
    }
  };

  return (
    <>
      <Header />
      <Grid
        container
        justify="center"
        alignItem="center"
        style={{ marginTop: 20 }}
      >
        <Text label="Hello world" />
        <Grid item style={{ border: "1px solid #d1d1d1" }}>
          <Typography>Current User: {currentUser}</Typography>
          <Button
            variant={"outlined"}
            onClick={() => {
              handleLanguage("javascript");
            }}
          >
            Javascript
          </Button>
          <Button
            variant={"outlined"}
            onClick={() => {
              handleLanguage("java");
            }}
          >
            Java
          </Button>
          <AceEditor
            mode={mode}
            theme={"github"}
            onChange={onChange}
            editorProps={{ $blockScrolling: true }}
            value={code}
          />
        </Grid>
      </Grid>
    </>
  );
};

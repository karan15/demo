import React from "react";
import Header from "../../component/header";
import { useSelector, useDispatch } from "react-redux";
import * as actionTypes from "../../redux/action";

const ContactUs = (props) => {
  const counter = useSelector((store) => {
    return store.counter;
  });
  const dispatch = useDispatch();

  const increment = () => {
    dispatch({ type: actionTypes.INCREMENT });
  };

  const decrement = () => {
    dispatch({ type: actionTypes.DECREMENT });
  };

  return (
    <>
      <Header />
      <h1>Contact Us</h1>
      {counter}
      <button onClick={increment}>INC</button>
      <button onClick={decrement}>DEC</button>
    </>
  );
};

export default ContactUs;

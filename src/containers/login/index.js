import React, { useEffect } from "react";
import { Grid, Typography, Button } from "@material-ui/core";
import firebase from "firebase";
import fire from "../../fire";

const Login = (props) => {
  const handleClick = () => {
    console.log("when handleClick called");
    const provider = new firebase.auth.GoogleAuthProvider();
    fire
      .auth()
      .signInWithPopup(provider)
      .then(function (result) {
        // This gives you a Google Access Token. You can use it to access the Google API.
        var token = result.credential.accessToken;
        // The signed-in user info.
        var user = result.user;
        console.log("in then");
        console.log(user);
        props.history.push("/");
        // ...
      })
      .catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        alert("login failed");
        // ...
      });

    console.log("at the end of function");
  };

  useEffect(() => {
    fire.auth().onAuthStateChanged(function (user) {
      if (user) {
        // User is signed in.
        var displayName = user.displayName;
        var email = user.email;
        var emailVerified = user.emailVerified;
        var photoURL = user.photoURL;
        var isAnonymous = user.isAnonymous;
        var uid = user.uid;
        var providerData = user.providerData;
        console.log(user.displayName);
        props.history.push("/");
        // ...
      } else {
        // User is signed out.
        // ...
      }
    });
  }, []);

  const logout = () => {
    fire
      .auth()
      .signOut()
      .then(function () {
        props.history.push("/login");
      })
      .catch(function (error) {
        // An error happened.
        alert("failed");
      });
  };

  return (
    <Grid>
      <Typography variant={"h4"}>Login Page</Typography>
      <Grid>
        <Button variant="contained" onClick={handleClick}>
          Sign In
        </Button>
      </Grid>
    </Grid>
  );
};

export default Login;

import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter, Route } from "react-router-dom";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { Provider } from "react-redux";

import Home from "./containers/home";
import ContactUs from "./containers/contactUs";
import Property from "./containers/property";
import CustomerSupport from "./containers/customerSupport";
import LoginScreen from "./containers/login";
import { CodeCollab } from "./containers/codeCollab";
import UsersList from "./containers/usersList";
import reducer from "./redux/reducer";

const store = createStore(reducer, applyMiddleware(thunk));

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <div>
        <Route exact path="/" component={Home} />
        <Route exact path="/contactUs" component={ContactUs} />
        <Route exact path="/property/:id" component={Property} />
        <Route exact path="/support" component={CustomerSupport} />
        <Route exact path="/login" component={LoginScreen} />
        <Route exact path="/codeCollab" component={CodeCollab} />
        <Route exact path="/usersList" component={UsersList} />
      </div>
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

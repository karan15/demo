export const INCREMENT = "increment";
export const DECREMENT = "decrement";
export const GET_USER_DATA_INITIATED = "users/initiated";
export const GET_USER_DATA_SUCCESS = "users/success";
export const GET_USER_DATA_FAIL = "user/fail";
export const GET_USER_DATA_CANCEL = "user/cancel";
export const LOGOUT = "logout";

import axios from "axios";

export const INCREMENT = "increment";
export const DECREMENT = "decrement";
export const GET_USER_DATA_INITIATED = "users/initiated";
export const GET_USER_DATA_SUCCESS = "users/success";
export const GET_USER_DATA_FAIL = "user/fail";

export const getUsers = () => {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_USER_DATA_INITIATED });
      const usersData = await axios.get(
        "https://randomuser.me/api/?&results=100"
      );
      dispatch({ type: GET_USER_DATA_SUCCESS, data: usersData.data.results });
    } catch (err) {
      dispatch({ type: GET_USER_DATA_FAIL, data: err });
    }
  };
};
